# Large Symbolics

Experimental set of symbolics for use at large sizes (64 and up).

## How to Edit

- Open sheet `large-icons.svg` in Inkscape
- Add/edit icons inside groups with invisible squares (which are the canvas for each individual icon)
- Convert strokes to paths, otherwise they won't render correctly
- Add metadata in `Ctrl+Shift+O` Object Properties dialog (Icon name in Title, tags in Label)
- Open sheet in Symbolic Preview
- Check if the icons render ok and are on the pixel grid
- Click "Export All" and export to the `assets` folder in the repo
- Commit and open a merge request
